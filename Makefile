# this seems to substitute just fine the old rake command, fine
all:
	conf.d/3000-network.sh

test:
	./test-with-shellcheck.sh
	# we ensure that temba is run in TEST mode
	shellspec --env TEST=true

.PHONY: coverage
# TODO fix coverage
coverage:
	shellspec --env TEST=true --kcov --kcov-options "--include-pattern=conf.d/0000-tembalib.sh"
